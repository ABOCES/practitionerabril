var express = require('express');
var port = process.env.PORT || 3000;
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaboc/collections/";
var mLabAPIKey= "apiKey=nFAgDFM8uYrS_0QRPo3zi60Nf-IbpHKl";
var requestJson = require('request-json');

app.listen(port);
console.log("API molona escuchando en el puerto " + port);

app.post("/apitechu/v1/login",
function(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log("email is" + req.body.email);
  console.log("password " + req.body.password);

  var msg;

  var Userlogado ={
    "email": req.body.email,
    "password": req.body.password
  }







  var users = require('./usuarios.json');
  for (user of users) {
    if (Userlogado.email == user.email) {
      if (Userlogado.password = user.password) {
        user.logged = true;
        writeUserDataToFile(users);
        msg = '{ "mensaje" : "Login correcto",  "idUsuario" :' + user.id +'}';
        break;
      }
      else {
        msg = '{ "mensaje" : "Login incorrecto"}';
      }
    }
    else {
      msg = '{ "mensaje" : "Login incorrecto"}';
    }
  }
  res.send(msg);
}
);

app.post("/apitechu/v2/login",
function(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log("email is" + req.body.email);
  console.log("password " + req.body.password);

  var msg;

  var Userlogado ={
    "email": req.body.email,
    "password": req.body.password
  }

  var query = 'q={"email": "' + Userlogado.email + '", "password": "' + Userlogado.password +'"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};
    console.log("ejecutado get");
    console.log(body[0].id);
    var iden = body[0].id;
    if (err){
      response = {
        "msg" : "Errror obtenido usuario."
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        var putBody = '{"$set":{"logged":true}}';
        console.log("usuario encontrado");
        httpClient.put("user?" + query + "&" +  mLabAPIKey, JSON.parse(putBody),
        function(err, resMLab2, body) {
          var response = {};
          if (err){
            response = {
              "msg" : "Error actualizando usuario."
            }
            res.status(500);
            res.send(response);
          } else {
            console.log("usuario logado");
            response = {
              "msg" : "Usuario logado",
              "id"  : iden
            }
            res.send(response);
          }
        });
      } else {
        response = {"msg" : "Usuario no encontrado."};
        res.status(404);
        res.send(response);
      }
    }
  });
});

app.post("/apitechu/v2/logout",
function(req, res) {
  console.log("POST /apitechu/v2/logout");
  console.log("id" + req.body.id);

  var msg;


  var query = 'q={"id":' + req.body.id + '}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};
    console.log("ejecutado get");
    if (err){
      response = {
        "msg" : "Errror obtenido usuario."
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        var putBody = '{"$unset":{"logged":""}}';
        httpClient.put("user?" + query + "&" +  mLabAPIKey, JSON.parse(putBody),
        function(err, resMLab2, body) {
          var response = {};
          if (err){
            response = {
              "msg" : "Error actualizando usuario."
            }
            res.status(500);
            res.send(response);
          } else {
            console.log("usuario actualizado");
            response = {
              "msg" : "Usuario actualizado"
            }
            res.send(response);
          }
        });
      } else {
        response = {"msg" : "Usuario no encontrado."};
        res.status(404);
        res.send(response);
      }
    }
  }
)
}
);



app.post("/apitechu/v1/logout",
function(req, res) {
  console.log("POST /apitechu/v1/logout");
  console.log("id" + req.body.id);

  var msg;
  var users = require('./usuarios.json');

  for (user of users) {
    if (req.body.id == user.id) {
      delete user.logged
      writeUserDataToFile(users);
      msg = '{ "mensaje" : "Logout correcto",  "idUsuario" :' + user.id +'}';
      break;
    }
    else {
      msg = '{ "mensaje" : "Logout incorrecto"}';
    }
  }
  res.send(msg);
}
);


app.get("/apitechu/v1",
function(req, res) {
  res.send({'msg': "Hola desde APITechU"});
  //var users = require('./usuario"Hola desde APITechU"s.json');
  //res.send(users)
}
);



app.get("/apitechu/v1/users",
function(req, res) {
  console.log("GET /apitechu/v1/users");
  res.sendFile('usuarios.json', {root: __dirname})
  //var users = require('./usuarios.json');
  //res.send(users)
}
);

app.post("/apitechu/v1/users",
function(req, res) {
  console.log("POST /apitechu/v1/users");
  console.log("first_namer is " + req.body.first_name);
  console.log("last_namer is " + req.body.last_name);
  console.log("country is " + req.body.country);


  var newUser ={
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "country": req.body.country
  };

  var users = require('./usuarios.json');
  users.push(newUser);
  writeUserDataToFile(users);
  console.log("Usuario guardado con exito");
  res.send({"msg" : "Usuario guardado con exito"});
}
);


app.delete("/apitechu/v1/users/:id",
function(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log(req.params.id);

  var users = require('./usuarios.json');
  users.splice(req.params.id -1, 1);
  writeUserDataToFile(users);
  console.log("Usuario borrado");
  res.send({"msg" : "Usuario borrado"});
}
)

function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf-8",
  function(err) {
    if (err)  {
      console.log(err);
    } else {
      console.log("Datos escritos en archivo");
    }
  }
);
}

app.post("/apitechu/v1/monstruo/:p1/:p2",
function(req, res) {
  console.log("Parametros");
  console.log(req.params);

  console.log("Query String");
  console.log(req.query);

  console.log("Body");
  console.log(req.body);

  console.log("headers");
  console.log(req.headers);
}
);


app.get("/apitechu/v2/users",
function(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("cliente http creado");

  httpClient.get("user?" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg" : "Errror obtenido usuarios"
    }
    res.send(response);
  }
)
}
);

app.get("/apitechu/v2/users/:id",
function(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  console.log(id);
  var query = 'q={"id":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("cliente http creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    // var response = !err ? body : {
    //     "msg" : "Errror obtenido usuario."
    //   }

    var response = {};

    if (err){
      response = {
        "msg" : "Errror obtenido usuario."
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      }
      else {
        response ={
          "msg" : "Usuario no encontrado"
        };
        res.status(404);
      }
    }
    res.send(response);
  }
)
}
);

app.get("/apitechu/v2/users/:id/accounts",
function(req, res) {
  console.log("GET /apitechu/v2/users/:id/accounts");

  var id = req.params.id;
  var query = 'q={"userid":' + id + '}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("cliente http creado");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    // var response = !err ? body : {
    //     "msg" : "Errror obtenido usuario."
    //   }

    var response = {};

    if (err){
      response = {
        "msg" : "Errror obtenido cuenta."
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      }
      else {
        response ={
          "msg" : "Cuenta no encontrada"
        };
        res.status(404);
      }
    }
    res.send(response);
  }
)
}
);
