var express = require('express');
var port = process.env.PORT || 3000;
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = "https://api.mlab.com/api/1/databases/apitechuaboc/collections/";
//var baseMLabURL = 'https://api.mlab.com/api/1/databases/apitechudvg5ed/collections/';
var mLabAPIKey= "apiKey=nFAgDFM8uYrS_0QRPo3zi60Nf-IbpHKl";
//var mLabAPIKey = "apiKey=PYq8LqFEw3tiRenrVOlDiLicbVQgWlRP";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto " + port);

app.post("/apitechu/v1/login",
function(req, res) {
  console.log("POST /apitechu/v1/login");
  console.log("email is" + req.body.email);
  console.log("password " + req.body.password);

  var msg;

  var Userlogado ={
    "email": req.body.email,
    "password": req.body.password
  }

  var users = require('./usuarios.json');
  for (user of users) {
    if (Userlogado.email == user.email) {
      if (Userlogado.password = user.password) {
        user.logged = true;
        writeUserDataToFile(users);
        msg = '{ "mensaje" : "Login correcto",  "idUsuario" :' + user.id +'}';
        break;
      }
      else {
        msg = '{ "mensaje" : "Login incorrecto"}';
      }
    }
    else {
      msg = '{ "mensaje" : "Login incorrecto"}';
    }
  }
  res.send(msg);
}
);

app.post("/apitechu/v2/login",
function(req, res) {
  console.log("POST /apitechu/v2/login");
  console.log("dni is:" + req.body.dni);
  console.log("password is: " + req.body.password);

  var msg;

  var Userlogado ={
    "dni": req.body.dni,
    "password": req.body.password
  }

  var query = 'q={"dni": "' + Userlogado.dni + '", "password": "' + Userlogado.password +'"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};
    console.log("ejecutado get");
    console.log(body);
    //var iden = body[0].dni;
    if (err){
      response = {
        "msg" : "Errror obtenido usuario."
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        var putBody = '{"$set":{"logged":true}}';
        console.log("usuario encontrado");
        httpClient.put("user?" + query + "&" +  mLabAPIKey, JSON.parse(putBody),
        function(err, resMLab2, body) {
          var response = {};
          if (err){
            response = {
              "msg" : "Error actualizando usuario."
            }
            res.status(500);
            res.send(response);
          } else {
            console.log("usuario logado");
            response = {
              "msg" : "Usuario logado",
              "dni" : Userlogado.dni
            }
            res.send(response);
          }
        });
      } else {
        response = {"msg" : "Usuario no encontrado."};
        res.status(404);
        res.send(response);
      }
    }
  });
});

app.post("/apitechu/v2/user",
function(req, res) {
  console.log("POST/apitechu/v2/user");
  console.log("dni is" + req.body.dni);
  console.log("nombre is" + req.body.name);
  console.log("apellidos is" + req.body.lastname);
  console.log("email is" + req.body.email);
  console.log("password is" + req.body.password);

  var msg;

  var userAlta ={
    "dni": req.body.dni,
    "name": req.body.name,
    "lastname": req.body.lastname,
    "email": req.body.email,
    "password": req.body.password
  };

  console.log(userAlta);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.post("user?" + mLabAPIKey, userAlta,
  function(err, resMLab, body) {
    if (err){
      response = {
        "msg" : "Error al insertar usuario."
      }
      res.status(500);
      res.send(response);
    } else {
      console.log("insert ejecutado");
      //console.log(resMLab);
      response = {
        "msg" : "Usuario dado de alta"
      }
      res.send(response);
    }
  });
});

app.delete("/apitechu/v2/user/:dni/account/:iban",
function(req, res) {
  console.log("delete/apitechu/v2/account/iban");
  console.log("iban is: " + req.params.iban);
  var msg;
  var dni = req.params.dni;
  console.log(dni);
  var accountBaja =[{}];
  var query = 'q={"iban": "' + req.params.iban + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.put("movement?" + query + "&" + mLabAPIKey, accountBaja,
  function(err, resMLab, body) {
    if (err){
      response = {
        "msg" : "Error al borrar movimientos."
      }
      res.status(500);
      res.send(response);
    } else {
      console.log("movimientos borrados de la cuenta solicitada");
      //console.log(resMLab);
      httpClient.put("account?" + query + "&" + mLabAPIKey, accountBaja,
      function(err, resMLab, body) {
        if (err){
          response = {
            "msg" : "Error al borrar cuenta."
          }
          res.status(500);
          res.send(response);
        } else {
          console.log("cuenta borrada");
          response = {
            "dni" : dni
          }
          res.send(response);
        }
      });
    }
  });
});

app.post("/apitechu/v2/user/:dni/account",
function(req, res) {
  console.log("POST/apitechu/v2/iban/account");
  console.log("iban is" + req.params.dni);
  var msg;

  var ccc = Math.floor(Math.random()*9999999999);
  var iban = Math.floor(Math.random()*99);
  var account = "ES"+iban+"01824000"+"23"+ccc;
  var importe = 0;
  console.log(account);

  var accountAlta ={
    "iban": account,
    "dni": req.params.dni,
    "balance": importe
  };

  console.log(accountAlta);

  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.post("account?" + mLabAPIKey, accountAlta,
  function(err, resMLab, body) {
    if (err){
      response = {
        "msg" : "Error al insertar cuenta."
      }
      res.status(500);
      res.send(response);
    } else {
      console.log(resMLab);
      console.log("insert ejecutado");
      response = {
        "dni" : accountAlta.dni
      }
      res.send(response);
    }
  });
});

app.post("/apitechu/v2/logout/:dni",
function(req, res) {
  console.log("POST /apitechu/v2/logout");
  console.log("dni" + req.params.dni);

  var msg;
  var query = 'q={"dni":"' + req.params.dni + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};
    console.log("ejecutado get del logout");
    if (err){
      response = {
        "msg" : "Error obteniendo usuario."
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        var putBody = '{"$unset":{"logged":""}}';
        httpClient.put("user?" + query + "&" +  mLabAPIKey, JSON.parse(putBody),
        function(err, resMLab2, body) {
          var response = {};
          if (err){
            response = {
              "msg" : "Error actualizando usuario."
            }
            res.status(500);
            res.send(response);
          } else {
            console.log("Usuario autenticado.");
            response = {
              "msg" : "Usuario autenticado."
            }
            res.send(response);
          }
        });
      } else {
        response = {"msg" : "Usuario no encontrado."};
        res.status(404);
        res.send(response);
      }
    }
  }
)
}
);

app.get("/apitechu/v2/users",
function(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("cliente http creado");

  httpClient.get("user?" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = !err ? body : {
      "msg" : "Errror obtenido usuarios"
    }
    res.send(response);
  }
)
}
);

app.get("/apitechu/v2/users/:dni",
function(req, res) {
  console.log("GET /apitechu/v2/users/:dni");

  var iden = req.params.dni;
  console.log(iden);
  //var query = 'q={"dni":' + iden + '}';
  var query = 'q={"dni":"' + iden + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("cliente http creado");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};

    if (err){
      response = {
        "msg" : "Errror obteniendo usuario."
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      }
      else {
        response ={
          "msg" : "Usuario no encontrado."
        };
        res.status(404);
      }
    }
    res.send(response);
  }
)
}
);

app.get("/apitechu/v2/users/:dni/accounts",
function(req, res) {
  console.log("GET /apitechu/v2/users/:dni/accounts");

  var dni = req.params.dni;
  var query = 'q={"dni":"' + dni + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente http creado.");

  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};

    if (err){
      response = {
        "msg" : "Errror obteniendo cuenta."
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      }
      else {
        response ={
          "msg" : "Cuenta no encontrada."
        };
        res.status(404);
      }
    }
    res.send(response);
  }
)
}
);

app.get("/apitechu/v2/users/:dni/accounts/:iban/movements",
function(req, res) {
  console.log("GET /apitechu/v2/users/:dni/accounts/:iban/movements");

  var dni = req.params.dni;
  var iban = req.params.iban;
  var query = 'q={"iban":"' + iban + '"}';

  var httpClient = requestJson.createClient(baseMLabURL);
  console.log("Cliente HTTP creado.");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};

    if (err){
      response = {
        "msg" : "Error obteniendo movimientos."
      }
      res.status(500);
    } else {
      if (body.length > 0) {
        response = body;
      }
      else {
        response ={
          "msg" : "Movimientos no encontrados."
        };
        res.status(404);
      }
    }
    res.send(response);
  }
)
}
);

app.post("/apitechu/v2/users/accounts/movements",
function(req, res) {
  var msg, movementAlta, httpClient;

  console.log("POST/apitechu/v2/users/accounts/movements");
  console.log("IBAN is " + req.body.iban);
  console.log("date is " + req.body.date);
  console.log("description is " + req.body.description);
  console.log("amount is " + req.body.amount);
  console.log("location is " + req.body.location);

  movementAlta = {
    "iban": req.body.iban,
    "date": req.body.date,
    "description": req.body.description,
    "amount": req.body.amount,
    "location": req.body.location
  };
  console.log(movementAlta);

  httpClient = requestJson.createClient(baseMLabURL);
  httpClient.post("movement?" + mLabAPIKey, movementAlta,
  function(err, resMLab, body) {
    if (err){
      response = {
        "msg" : "Error al insertar movimiento."
      }
      res.status(500);
      res.send(response);
    } else {
      console.log("Inserción realizada.");
    }
  });

  var query = 'q={"iban": "' + req.body.iban + '"}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
  function(err, resMLab, body) {
    var response = {};
    console.log(body);
    if (err){
      response = {
        "msg" : "Errror obteniendo cuenta."
      }
      res.status(500);
      res.send(response);
    } else {
      if (body.length > 0) {
        var putBody = '{"$set":{"balance":' + (body[0].balance + req.body.amount) + '}}';
        console.log("Cuenta encontrada.");
        httpClient.put("account?" + query + "&" +  mLabAPIKey, JSON.parse(putBody),
        function(err, resMLab2, body) {
          var response = {};
          if (err){
            response = {
              "msg" : "Error actualizando cuenta."
            }
            res.status(500);
            res.send(response);
          } else {
            console.log("Cuenta actualizada.");
            response = {
              "msg" : "Cuenta actualizada",
              "IBAN" : req.body.iban
            }
            res.send(response);
          }
        });
      } else {
        response = {"msg" : "Cuenta no encontrada."};
        res.status(404);
        res.send(response);
      }
    }
  });
});
